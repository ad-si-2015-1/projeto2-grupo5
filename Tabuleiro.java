/**
 *
 * @author Mauro BRaga
 */
package memoria;

import java.util.Random;

public class Tabuleiro {
    
    int[][] tabuleiro = new int[4][4];
    int[][] respostaJogador = new int [4][4];
    Random gerador = new Random();

    public Tabuleiro() {
               
        for(int i=0;i<4;i++){
            for(int j=0; j<4;j++){
                this.tabuleiro[i][j] = gerador.nextInt(10);                
            }
        }        
    }

    public void imprimirTabuleiro() {
        for(int i=0;i<4;i++){
            for(int j=0; j<4;j++){
                System.out.print(tabuleiro[i][j]+" ");
            }
            System.out.println();
        }
    }
}
