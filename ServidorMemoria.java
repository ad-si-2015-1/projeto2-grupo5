/**
 *
 * @author Mauro BRaga
 */

package memoria;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author MAURO
 */
public class ServidorMemoria {
    
    ArrayList<ServerGame> servidores =
            new ArrayList<ServerGame>();
    int porta = 6789;
    int status = 0;    
    ServerSocket conexaoServidor;
    Tabuleiro tabuleiro;
    
    public ServidorMemoria(int porta) {
        try {
            this.conexaoServidor = new ServerSocket(porta);
            System.out.println("Aguardando conexoes na porta " + porta);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
      
    ServerSocket getConexaoServidor() {
        return conexaoServidor;
    }
    
    ArrayList<ServerGame> getServidores() {
        return servidores;
    }
    
    Tabuleiro getTabuleiro() {
        return tabuleiro;
    }

    
    Tabuleiro criarTabuleiro() {
        for (ServerGame servidor : getServidores()) {
            tabuleiro.imprimirTabuleiro();            
        }
        return getTabuleiro();
    }
    
    int getStatus() {
        return status;
    }
    
    void iniciar() {
        status = 1;
        tabuleiro = new Tabuleiro();
        for (ServerGame servidor : servidores) {
            servidor.notificar("O jogo foi iniciado!\n");
        }
    }
    
    public static void main(String[] args) throws IOException{
        ServidorMemoria servidorAMU = new ServidorMemoria(6789);
        while (true) {
            if (servidorAMU.getStatus() == 0) {
                Socket conexao = servidorAMU.getConexaoServidor().accept();
                ServerGame st = new ServerGame(conexao, servidorAMU);
                servidorAMU.getServidores().add(st);
                st.start();
            }
        }
    }
    
}
