/**
 *
 * @author Mauro BRaga
 */

package memoria;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class ServerGame extends Thread {
    
    /***************************************************************************
     *                        DECLARAÇÃO DE ATRIBUTOS                          *
     ***************************************************************************
     */
    
    Socket conexao;
    String nome;
    int pontuacao = 0;
    ServidorMemoria server;
    BufferedReader doCliente;
    DataOutputStream paraCliente;
    int[][] tabuleiro = new int[4][4];
    int[][] respostaJogador = new int [4][4];
    Random gerador = new Random();
    Scanner entrada = new Scanner(System.in);
    
    
    /***************************************************************************
     *                           MÉTODO CONSTRUTOR                             *
     ***************************************************************************
     */
    public ServerGame(Socket conexao, ServidorMemoria server) {
        try {
            this.conexao = conexao;
            this.server = server;
            doCliente = new BufferedReader(
                    new InputStreamReader(
                            conexao.getInputStream()));
            paraCliente = new DataOutputStream(
                    conexao.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void notificar(String mensagem) {
        try {
            paraCliente.writeBytes(mensagem);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void pontuacao() {
        try {
            if (pontuacao != 0) {
                paraCliente.writeBytes("Nova Pontuacao: " + pontuacao + "\n");
            } else {
                paraCliente.writeBytes("Pontuacao: " + pontuacao + "\n");
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
        
    /**
     * Método responsável por guardar a resposta do jogador
     */
    public void setResposta() throws IOException{
        
        System.out.println("Informe sua resposta!");
        for(int i=0;i<4;i++){
            for(int j=0; j<4;j++){
                System.out.println("Linha: "+ i +" | Coluna: " +j+ "   :");
                this.respostaJogador[i][j] = Integer.parseInt(doCliente.readLine());
            }
        }  
        
    }
    
    /**
     * Método responsável por comparar as respostas e atribuir a pontuação
     * @param tabuleiro
     * @param resposta 
     */
    public void compararResposta(int[][] tabuleiro, int[][] resposta){
        
        for(int i=0;i<4;i++){
            for(int j=0; j<4;j++){
                if(tabuleiro[i][j] != resposta[i][j]){
                    this.pontuacao--;
                }else{
                    this.pontuacao++;
                }
            }
        }  
    }
    
    public void run() {
        try {
            paraCliente.writeBytes("Forneca seu nome: ");
            nome = doCliente.readLine();

            while (server.getStatus() == 0) {
                paraCliente.writeBytes("O jogo ainda nao comecou. "
                        + "Digite '/iniciar' para comecar ou "
                        + "'/listar' para listar os jogadores\n");
                String comando = doCliente.readLine();
                if (comando.equals("/listar")) {
                    ArrayList<ServerGame> servidores = server.getServidores();
                    for (ServerGame servidorThread : servidores) {
                        paraCliente.writeBytes(servidorThread.nome + "\n");
                    }
                } else {
                    server.iniciar();
                }
            }
            
            Tabuleiro tabuleiro;
            
            while (server.getStatus() == 1) {

                tabuleiro = server.criarTabuleiro();
                this.setResposta();
                this.compararResposta(tabuleiro, respostaJogador);
                pontuacao();

            }
            conexao.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void compararResposta(Tabuleiro tabuleiro, int[][] respostaJogador) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
