### Documento de Especificação de Requisitos
### Jogo da Memória Multiusuário em Sockets


#### Universidade Federal de Goiás - UFG
#### Instituto de Informática - INF
#### Sistemas de Informação
#### Aplicações Distribuídas
#### 1º semestre/ 2015
-------------------------------------------------------------
> **Professor:**
- Marcelo Akira Inuzuka

> **Grupo:**
- Allan Braga
- Farid Chaud
- Mauro Henrique
- Miriã Laís


-------------------------------------------------------------

#### 1. Introdução

**1.1. Propósito do documento de requisitos**

Este documento tem como propósito especificar as funcionalidades, requisitos funcionais, não funcionais e seus respectivos  de uso, para fins de avaliação de viabilidade para futuro desenvolvimento. 


**1.2. Escopo do produto**

O Jogo da Memória tem consiste em encontrar "peças" semelhantes, ou seja, o jogador deve selecionar duas peças,  com o intuito de que elas sejam idênticas, sendo que há pares de cada peça.


**1.3. Definições, acrônimos e abreviações**
- RF - Requisitos Funcionais;
- RNFP - Requisitos não Funcionais do Produto;
- RNFO - Requisitos não Funcionais Organizacionais;
- RNFE - Requisitos não Funcionais Externos;
- CSU - Caso de Uso.

#### 2. Descrição Geral

**2.1. Perspectiva do Software**

O software deve funcionar tanto em ambiente Windows, quanto Linux. É contituído pelos relacionamentos de seis agentes, que se comunicam
por meio de sockets.

**2.2. Funções do Sofware**

- Identificar jogadores;
- Identificar as peças;
- Conectar jogadores à um servidor;
- Permitir jogadas sucessivas de um mesmo jogador em caso de acertos;
- Passar a vez para o outro jogador, caso as peças sejam distintas;
- Exibir pontuação.

**2.3. Características dos Jogadores**

Usuários comuns que queiram jogar.

**2.4. Restrições Gerais**

A interface gráfica não será priorizada na construção do software.


#### 3. Requisitos do Sistema

**3.1. Requisitos Funcionais (RF)**

Requisitos Funcionais descrevem explicitamente as funcionalidades e serviços do sistema. Nesta seção será documentado os principais requisitos funcionais da aplicação:

RF001 - Utilizar sockets.

RF002 - O jogo deve possuir no mínimo dois jogadores.

RF003 - Jogo com matriz 4x4 com números de 0 a 9, gerados aleatoriamente.

RF004 - Um jogador deve receber a escolha do outro.

RF005 - Quando um jogador acertar, deve ter sua pontuação somada.

RF006 - Quando um jogado errar, deve ter sua pontuação subtraída.

RF007 - Quando um jogador acertar, deve jogar novamente.

RF008 - Quando um jogador errar, deve passar a vez.

FR009 - O jogo deve exibir a pontuação de cada jogador, para ambos os jogadores.

RF010 - Quando todas as peças forem encontradas, vence o que tiver encontrado o maior número de pares.

RF011 - O sistema deve mostrar os pontos dos jogadores na medida em que forem sendo incrementados.

RF012 - O jogador 1 é o que inicia a partida.

RF013 - Jogador 1 só pode enviar a primeira tentativa quando o jogador 2 também estiver conectado.


**3.2. Requisitos não Funcionais**

Requisitos não funcionais diferentemente dos funcionais, não especifica o que o sistema fará, mas como ele fará. Abordam aspectos de qualidade importantes em um sistema de software. Nesta seção iremos identificar os principais requisitos não funcionais da aplicação:

**3.2.1. Requisitos Não Funcionais do Produto (RNFP)**

RNFP001 - Os jogadores deverão se comunicar por meio de protocolos de rede (TCP e/ou UDP).

RNFP002 - O sistema não apresentará uma interface gráfica.

RNFP003 - O sistema deve informar aos jogadores quando é a sua vez de jogar ou de passar a vez.


**3.2.2. Requisitos Não Funcionais Organizacionais (RNFO)**

RNFO001 - A aplicação será desenvolvida em linguagem Java.

RNFO002 - A aplicação será desenvolvida na IDE Netbeans.

RNFO003 - A documentação do projeto será realizada em paralelo ao desenvolvimento.


**3.2.3. Requisitos Não Funcionais Externos (RNFE)**

RNFE001 - Uso do GitLab como ferramenta colaborativa do projeto.

RNFE002 - Uso das ferramentas de diagramação do site https://www.gliffy.com.


### 4. Casos de Uso (CSU)

**Atores**
- Jogador 1;
- Jogador 2.

**CSU001 – Conexão**

Ator: Jogador 1 e Jogador 2

Descrição: Jogadores se conectam ao servidor.


**CSU002 – Iniciar jogo**

Atores: Jogador 1

Descrição: Após ambos os jogadores se conectarem ao servidor, o jogador 1 inicia o jogo.


**CSU003 – Escolher peças**

Atores: Jogador 1 e Jogador 2

Descrição: De acordo com a vez de jogar, cada jogador escolhe duas peças, se as peças coincidirem ele joga novamente, se forem diferentes passa a vez para o outro jogador.


**CSU004 – Jogar novamente**

Atores: Jogador 1 e Jogador 2

Descrição: Ação realizada enquanto o jogador for acertando as combinações, até todas os pares terem sido encontrados.


**CSU005 – Passar a vez**

Atores: Jogador 1 e Jogador 2

Descrição: Ação acontece quando um jogador erra a combinação das peças escolhidas.


**CSU006 – Vencer jogo**

Atores: Jogador 1 ou Jogador 2

Descrição: O jogador que encontrar o maior número de pares vence o jogo.


**CSU007 – Encerrar jogo**

Atores: Jogador 1 ou Jogador 2

Descrição: O jogador que vencer o jogo, pode optar por iniciar nova partida, ou encerrar o jogo (conexão). O jogador que perder pode deixar o jogo sem nenhum impedimento.


### 5. Diagramas

**5.1. Diagrama de Raias**

Arquivo anexado.


**5.2. Diagrama de Estados**

Arquivo anexado.